#include "LinkedList.h"
#include <iostream>
using namespace myll;

LinkedList::LinkedList(int *v, int n){
		this->head = NULL;
		for (int i = 0; i < n; i++){
				this->Insert(v[i]);
		}
		this->ReverseList();
}

LinkedList::LinkedList(){
		this->head = NULL;
}

void LinkedList::Insert(int v){
		Node *t = new Node(v, this->head);
		this->head = t;
}

Node* LinkedList::Head(){
		return this->head;
}

void LinkedList::Delete(int v){
		Node *t = this->Head();
		if (t->Value() == v){
				this->head = t->Next();
				return;
		}
		while(t->Next()->Value() != v && t->Next() != NULL){
				t = t->Next();
		}
		if(t->Next() == NULL){
				return;
		}
		t->SetNext(t->Next()->Next());
}

void LinkedList::ReverseList(){
		Node *t = this->Head();
		Node *n = NULL;
		Node *h;
		while(t != NULL){
				h = t->Next();
				t->SetNext(n);
				n = t;
				t = h;
		}
		this->head = n;
}


void LinkedList::Push(int v){
		this->Insert(v);
}

Node* LinkedList::Pop(){
		Node* h = this->Head();
		this->head = h->Next();
		return h;
}

void LinkedList::Enqueue(int v){
		this->ReverseList();
		this->Push(v);
		this->ReverseList();
}

Node* LinkedList::Dequeue(){
		return this->Pop();
}
