#include "Node.h"
using namespace myll;
Node::Node(int v, Node *n)
{
		this->value = v;
		this->next  = n;
}

Node* Node::Next(){
		return this->next;
}
 int Node::Value(){
		 return this->value;
 }

void Node::SetNext(Node* n){
		this->next = n;
}

void Node::SetValue(int v){
		this->value = v;
}
