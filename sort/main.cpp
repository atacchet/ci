#include <iostream>
#include "Array.h"

int main(void){
//		int v[6] = {1,3,5,2,4,6};
		int v[10] = {1,7,9,3,10,5,2,4,8,6};
		Array *A = new Array(v,10);
		cout << "Array Constructor OK!" << endl;
		for(int i = 0; i < A->Size(); i++){
				cout << A->Value(i) << endl;
		}

//		A->Merge(0,2,A->Size());
		A->MergeSort();
		cout << "Sort OK!" << endl;
		for(int i = 0; i < A->Size(); i++){
				cout << A->Value(i) << endl;
		}

		return 0;
}
