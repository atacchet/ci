#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "Array.h"

Array::Array(int *v, int n){
		this->v = (int *) calloc(n, sizeof(int));
		for(int i = 0; i < n; i++){
				this->v[i] = v[i];
		}
		this->n = n;
}

void Array::qswap(int i, int j){
		int tmp = this->v[i];
		this->v[i] = this->v[j];
		this->v[j] = tmp;
}

int Array::Value(int i) const {
		return this->v[i];
}

int Array::Size() const {
		return this->n;
}

void Array::Merge(int p, int q, int r){
		int i;
		int j;
		int k;
		int nl = q - p + 1;
		int nr = r - q;
		int *L = (int *) calloc(nl+1, sizeof(int));
		int *R = (int *) calloc(nr+1, sizeof(int));
		memcpy(L, this->v + p,  	nl * sizeof(int));
		memcpy(R, this->v + q + 1, 	nr * sizeof(int));
		L[nl] = R[nr] = max(L[nl-1], R[nr-1]) + 1;
		for(i = 0, j = 0, k = p; k <= r; k++){
				if(L[i] <= R[j]){
						this->v[k] = L[i];
						i++;
				}
				else{
						this->v[k] = R[j];
						j++;
				}
		}
		free(L);
		free(R);
}

void Array::MergeSort(){
		this->MergeSort(0,this->Size());
}

void Array::MergeSort(int p, int r){
		int q;
		if(p < r){
				q = (p+r) / 2;
				this->MergeSort(p,q);
				this->MergeSort(q+1,r);
				this->Merge(p,q,r);
		}
}

int Array::Partition(int p, int r){
		int i = p - 1;
		int j;
		int x = this->Value(r);
		for(j = p; j < r; j++){
				if(this->Value(j) <= x){
						i++;
						this->qswap(i,j);
				}
		}
		qswap(i + 1, r);
		return i + 1;
}


void Array::QuickSort(){
		this->MergeSort(0, this->Size());
}

void Array::QuickSort(int p, int r){
		int q;
		if (p < r){
				q = this->Partition(p, r);
				this->QuickSort(p, q - 1);
				this->QuickSort(q + 1, r);
		}
}
