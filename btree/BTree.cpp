#include <iostream>
#include "BTree.h"

void BTree::Insert(int v){
	BTNode *r = this->Root();
	BTNode *t = NULL;
	while (r != NULL){
			t = r;
			if(v < r->Value()){
					r = r->Child(0);
			}
			else{
					r = r->Child(1);
			}
	}
	if(t == NULL){
			this->SetRoot(v);
	}
	else if(v < t->Value()){
			t->SetChild(new BTNode(v, NULL, NULL), 0);
	}
	else{
			t->SetChild(new BTNode(v, NULL, NULL), 1);
	}
}

void BTree::SetRoot(int v){
		this->root = new BTNode(v, NULL, NULL);
}

BTNode* BTree::Root(){
		return this->root;
}

BTree::BTree(int *v, int n){
		this->root = NULL;
		for (int i = 0; i < n; i++){
				this->Insert(v[i]);
		}
}

void BTree::TreeWalk(){
		this->TreeWalk(this->Root());
}

void BTree::TreeWalkPre(){
		this->TreeWalkPre(this->Root());
}

void BTree::TreeWalkPost(){
		this->TreeWalkPost(this->Root());
}
void BTree::TreeWalk(BTNode *r){
		if (r != NULL){
			this->TreeWalk(r->Child(0));
			cout << r->Value() << endl;
			this->TreeWalk(r->Child(1));
		}
}

void BTree::TreeWalkPre(BTNode *r){
		if(r != NULL){
				cout << r->Value() << endl;
				this->TreeWalkPre(r->Child(0));
				this->TreeWalkPre(r->Child(1));
		}
}

void BTree::TreeWalkPost(BTNode *r){
		if(r != NULL){
				this->TreeWalkPre(r->Child(0));
				this->TreeWalkPre(r->Child(1));
				cout << r->Value() << endl;
		}
}
