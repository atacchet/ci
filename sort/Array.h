#ifndef __ARRAY_H__
#define __ARRAY_H__

using namespace std;

class Array {
		private:
				int *v;
				int n;
				void qswap(int i, int j);
				void MergeSort(int p, int r);
				void Merge(int p, int q, int r);
				void QuickSort(int p, int r);
				int Partition(int p, int r);
		public:
				Array(int *v, int n);
				int Value(int i) const;
				int Size() const;
				void MergeSort();
				void QuickSort();
};
#endif
