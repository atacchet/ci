#ifndef __BTNODE_H__
#define __BTNODE_H__
using namespace std;
class BTNode;
class BTNode {
		private:
				BTNode *left;
				BTNode *right;
				int value;
		public:
				BTNode(int v, BTNode *l, BTNode* r);
				int Value();
				BTNode* Child(int s);
				void SetValue(int v);
				void SetChild(BTNode *n, int s);
};
#endif
