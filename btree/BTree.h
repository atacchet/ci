#ifndef __BTREE_H__
#define __BTREE_H__
#include "BTNode.h"
using namespace std;

class BTree {
		private:
				BTNode *root;
				void SetRoot(int v);
				void TreeWalk(BTNode *);
				void TreeWalkPre(BTNode *);
				void TreeWalkPost(BTNode *);
		public:
				BTree(int *v, int n);
				void Insert(int v);
				BTNode* Root();
				void TreeWalk();
				void TreeWalkPre();
				void TreeWalkPost();
};
#endif
