#ifndef __MAXHEAP_H__
#define __MAXHEAP_H__

#include <iostream>
using namespace std;

class MaxHeap {
		private:
				int *v;
				int n;
				void qswap(int, int);
		public:
				MaxHeap(int *v, int n);
				int Left(int i) const;
				int Right(int i) const;
				int Parent(int i) const;
				int Value(int i) const;
				int Size() const;
				void HeapSort();
				void MaxHeapify(int i);
};

#endif
