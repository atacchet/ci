#ifndef __NODE_H__
#define __NODE_H__
namespace myll{
	class Node {
			private:
					Node *next;
					int value;
			public:
					Node(int v, Node *n);
					int Value();
					Node* Next();
					void SetValue(int v);
					void SetNext(Node *n);
	};
}
#endif
