#include <iostream>
#include "LinkedList.h"

using namespace myll;

int main(void){
		int i;
		Node *t;
		LinkedList *ll = new LinkedList();
		for(i = 0; i < 10; i++){
				ll->Enqueue(i);
		}
		for(i = 0; i < 10; i++){
				t = ll->Dequeue();
				std::cout << t->Value() << "\n";
		}
		ll = new LinkedList();
		for(i = 0; i < 10; i++){
				ll->Push(i);
		}
		for(i = 0; i < 10; i++){
				t = ll->Pop();
				std::cout << t->Value() << "\n";
		}
		return 0;
}



