#include "BTNode.h"
using namespace std;

BTNode::BTNode(int v, BTNode* l, BTNode *r){
		this->SetValue(v);
		this->SetChild(l, 0);
		this->SetChild(r, 1);
}

void BTNode::SetValue(int v){
		this->value = v;
}

void BTNode::SetChild(BTNode *n, int s){
		if(s){
				this->right = n;
		}
		else{
				this->left = n;
		}
}

int BTNode::Value(){
		return this->value;
}

BTNode* BTNode::Child(int s){
		if(s){
				return this->right;
		}
		else{
				return this->left;
		}
}
