#ifndef __LINKEDLIST_H__
#define __LINEDLIKST_H__
#include "Node.h"


namespace myll{
	class LinkedList {
			private:
					Node* head;
			public:
					LinkedList(int *v, int n);
					LinkedList();
					Node* Head();
					void Delete(int v);
					void Insert(int v);
					void ReverseList();
					void Push(int v);
					Node* Pop();
					void Enqueue(int v);
					Node* Dequeue();
	};
}
#endif
