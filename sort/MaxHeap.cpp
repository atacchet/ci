#include <iostream>
#include <stdlib.h>
#include "MaxHeap.h"

MaxHeap::MaxHeap(int *v, int n){
		this->v = (int *) calloc(n, sizeof(int));
		this->n = n;
		for(int i = 0; i < n; i++){
				this->v[i] = v[i];
		}
		for(int i = this->Size()/2; i >= 0; i--){
				this->MaxHeapify(i);
		}
}

int MaxHeap::Left(int i) const {
		return 2*i;
}

int MaxHeap::Right(int i) const {
		return 2*i + 1;
}

int MaxHeap::Parent(int i) const {
		return (i / 2);
}

void MaxHeap::MaxHeapify(int i){
		int l = this->Left(i);
		int r = this->Right(i);
		int largest;
		if(l < this->Size() && this->Value(l) > this->Value(i)){
				largest = l;
		}
		else{
				largest = i;
		}
		if(r < this->Size() && this->Value(r) > this->Value(largest)){
				largest = r;
		}
		if (largest != i){
				this->qswap(i,largest);
				this->MaxHeapify(largest);
		}

}


void MaxHeap::qswap(int i, int largest){
		int tmp = this->v[largest];
		this->v[largest] = this->v[i];
		this->v[i] = tmp;
}

int MaxHeap::Value(int i) const {
		return this->v[i];
}

int MaxHeap::Size() const {
		return this->n;
}

void MaxHeap::HeapSort(){
		int s = this->Size();
		for(int i = s-1; i >= 0; i--){
				this->qswap(0,i);
				this->n = this->n - 1;
				this->MaxHeapify(0);
		}
		this->n = s;
}

